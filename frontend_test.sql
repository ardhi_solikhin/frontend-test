-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2020 at 08:09 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `frontend_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `discount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `user_count` int(11) NOT NULL,
  `feature` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`feature`)),
  `rating` int(11) DEFAULT NULL,
  `bonus_feature` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`bonus_feature`)),
  `discount_percentage` int(11) DEFAULT NULL,
  `best_seller` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `type`, `discount`, `price`, `user_count`, `feature`, `rating`, `bonus_feature`, `discount_percentage`, `best_seller`) VALUES
(1, 'Bayi', 19900, 14900, 938, '[{\"value\": \"0.5X RESOURCE POWER\", \"name\": \"\"},{\"value\": \"500 MB\", \"name\": \"Disk Space\"},{\"value\": \"Unlimited\", \"name\": \"Bandwidth\"},{\"value\": \"Unlimited\", \"name\": \"Databases\"},{\"value\": \"1\", \"name\": \"Domain\"},{\"value\": \"Instant\", \"name\": \"Backup\"},{\"value\": \"Unlimited SSL\", \"name\": \"Gratis Selamanya\"}]', NULL, '{\"value\": \"\", \"name\": \"\"}', NULL, 0),
(8, 'Pelajar', 46900, 23450, 4168, '[{\"value\":\"1X RESOURCE POWER\",\"name\":\"\"},{\"value\":\"Unlimited\",\"name\":\"Disk Space\"},{\"value\":\"Unlimited\",\"name\":\"Bandwidth\"},{\"value\":\"Unlimited\",\"name\":\"POP3 Email\"},{\"value\":\"Unlimited\",\"name\":\"Databases\"},{\"value\":\"10\", \"name\":\"Addon Domains\"},{\"value\":\"Instant\", \"name\":\"Backup\"},{\"value\":\"Domain Gratis\", \"name\":\"Selamanya\"},{\"value\":\"Unlimited SSL\", \"name\":\"Gratis Selamanya\"}]', NULL, '{ \"value\": \"\", \"name\": \"\" }', NULL, 0),
(9, 'Personal', 58900, 38900, 10017, '[{ \"value\": \"2X RESOURCE POWER\", \"name\": \"\" },{ \"value\": \"Unlimited\", \"name\": \"Disk Space\" },{ \"value\": \"Unlimited\", \"name\": \"Bandwidth\" },{ \"value\": \"Unlimited\", \"name\": \"POP3 Email\" },{ \"value\": \"Unlimited\", \"name\": \"Databases\" },{ \"value\": \"Unlimited\", \"name\": \"Addon Domains\" },{ \"value\": \"Instant\", \"name\": \"Backup\" },{ \"value\": \"Domain Gratis\", \"name\": \"Selamanya\" },{ \"value\": \"Unlimited SSL\", \"name\": \"Gratis Selamanya\" },{ \"value\": \"Private\", \"name\": \"Name Server\" },{ \"value\": \"SpamAssasin\", \"name\": \"Mail Protection\" }]', NULL, '{ \"value\": \"\", \"name\": \"\" }', NULL, 1),
(10, 'Bisnis', 109900, 65900, 3552, '[{ \"value\": \"3X RESOURCE POWER\", \"name\": \"\" },{ \"value\": \"Unlimited\", \"name\": \"Disk Space\" },{ \"value\": \"Unlimited\", \"name\": \"Bandwidth\" },{ \"value\": \"Unlimited\", \"name\": \"POP3 Email\" },{ \"value\": \"Unlimited\", \"name\": \"Databases\" },{ \"value\": \"Unlimited\", \"name\": \"Addon Domains\" },{ \"value\": \"Magic Auto\", \"name\": \"Backup & Restore\" },{ \"value\": \"Domain Gratis\", \"name\": \"Selamanya\" },{ \"value\": \"Unlimited SSL\", \"name\": \"Gratis Selamanya\" },{ \"value\": \"Private\", \"name\": \"Name Server\" },{ \"value\": \"Prioritas\", \"name\": \"Layanan Support\" }]', 5, '{ \"value\": \"SpamExpert\", \"name\": \"Pro Mail Protection\" }', 40, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
