<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $set_data = [];

        foreach ($this->collection as $key => $value) {
            $set_data[$key] = array(
                'id' => $value['id'],
                'type' => $value['type'],
                'discount' => $value['discount'],
                'price' => $value['price'],
                'user_count' => $value['user_count'],
                'feature' => json_decode($value['feature']),
                'rating' => $value['rating'],
                'bonus_feature' => json_decode($value['bonus_feature']),
                'discount_percentage' => $value['discount_percentage'],
                'best_seller' => $value['best_seller']
            );
        };

        return [
            'data' => $set_data
        ];
    }
}
