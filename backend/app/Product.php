<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['id', 'type', 'discount', 'price', 'user_count', 'feature', 'rating', 'bonus_feature', 'discount_percentage', 'best_seller'];
}
